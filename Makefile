TARFILE=baduki-0.2.9.tar.gz
TARDIR=baduki-0.2.9

INSTALL_PREFIX=debian/tmp/usr

DOC_DIR=$(INSTALL_PREFIX)/share/doc/baduki

CONFIGURE_FLAG=--prefix=/usr --bindir=/usr/games
MAKE_DEFINES=pkgdatadir=/usr/share/games/baduki

all: build

unpack: unpack-stamp
unpack-stamp: $(TARFILE)
	-rm -rf $(TARDIR)
	tar zxf $(TARFILE)
	for P in `ls patches/*.diff 2>/dev/null`; do \
	    (cd $(TARDIR) && patch -p1) < $$P; \
	done
	touch $@

build: build-stamp
build-stamp: unpack-stamp
	install -d build/baduki
	cd build/baduki && ../../$(TARDIR)/configure $(CONFIGURE_FLAG)
	cd build/baduki && make $(MAKE_DEFINES)

	touch $@

install: install-stamp
install-stamp: build-stamp
	P=`pwd`/$(INSTALL_PREFIX) && cd build/baduki && make install \
		prefix=$$P \
		bindir=$$P/games \
		pkgdatadir=$$P/share/games/baduki

#	# install documents
	install -d $(DOC_DIR)
	install -m644 $(TARDIR)/ChangeLog \
		$(DOC_DIR)/changelog
	install -m644 $(TARDIR)/README $(DOC_DIR)/
	install -m644 $(TARDIR)/README.kr $(DOC_DIR)/
	install -m644 $(TARDIR)/ChangeLog.kr \
		$(DOC_DIR)/changelog.kr
	install -m644 $(TARDIR)/NEWS $(DOC_DIR)/
	install -m644 $(TARDIR)/NEWS.kr $(DOC_DIR)/
	install -m644 $(TARDIR)/PATTERNS $(DOC_DIR)/

	touch $@

clean:
	rm -rf build 
	rm -rf $(TARDIR)
	rm -f build*-stamp install*-stamp unpack-stamp
	rm -f *~ patches/*~

